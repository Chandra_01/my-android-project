package com.google.nateriver.webservice.webservice;

/**
 * Created by Nate River on 3/26/2018.
 */

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit2.Call;


public class REST {

    public static String BASEURL="https://wallezzdev.com";

    public interface api_get {
        @Headers("API_KEY: NDEzODA2N2QzYzhhYzNmNzhmOGMyMDU1YTU1N2IyMjBjNGY0NzdlMzpiMmZmMGQzMWZkNDkyZTZhMDk3MGU2N2Y5MTU2ZjMxOGZmNTZmNzhj")
        @GET("/getProfile") // specify the sub url for the base url
        void getData(Callback<POJO> response); // will give you the json data
    }

    public interface api_post {
        @Headers("API_KEY: NDEzODA2N2QzYzhhYzNmNzhmOGMyMDU1YTU1N2IyMjBjNGY0NzdlMzpiMmZmMGQzMWZkNDkyZTZhMDk3MGU2N2Y5MTU2ZjMxOGZmNTZmNzhj")
        @FormUrlEncoded
        @POST("/getProfile") // specify the sub url for the base url
        void insertUser(@Field("nama") String name,
                        @Field("jenis_kelamin") String gender,
                        Callback<POJOPost> response);

//        Call<POJOPost> savePost(@Field("nama") String name,
//                                @Field("jenis_kelamin") String gender);
    }
}
