package com.google.nateriver.gratze;


import android.os.Bundle;
import android.support.constraint.solver.ArrayLinkedVariables;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.amigold.fundapter.BindDictionary;
import com.amigold.fundapter.FunDapter;
import com.amigold.fundapter.extractors.StringExtractor;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PrimaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrimaryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private final String TAG = this.getClass().getSimpleName(); /*hanya untuk print di log*/
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PrimaryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PrimaryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PrimaryFragment newInstance(String param1, String param2) {
        PrimaryFragment fragment = new PrimaryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Log.d(TAG, mParam1); /*hanya untuk print di log*/
        Toast.makeText(getActivity(),mParam1,Toast.LENGTH_SHORT).show(); /*untuk Toast fragment*/
        View view = inflater.inflate(R.layout.fragment_primary, container, false);

        final ArrayList<Product> products = new ArrayList<>();
        Product p1 = new Product("Coke", 100, 0.5);
        Product p2 = new Product("Pepsi", 150, 0.5);
        Product p3 = new Product("Spirit", 200, 0.5);
        Product p4 = new Product("Cocktail", 250, 0.5);
        Product p5 = new Product("Fanta", 300, 0.5);
        Product p6 = new Product("Juice", 350, 0.5);

        products.add(p1);
        products.add(p2);
        products.add(p3);
        products.add(p4);
        products.add(p5);
        products.add(p6);

        BindDictionary<Product> dictionary = new BindDictionary<>();
        dictionary.addStringField(R.id.tvName, new StringExtractor<Product>() {
            @Override
            public String getStringValue(Product product, int position) {
                return product.getName();
            }
        });
        dictionary.addStringField(R.id.tvQty, new StringExtractor<Product>() {
            @Override
            public String getStringValue(Product product, int position) {
                return "" + product.getQty();
            }
        });
        dictionary.addStringField(R.id.tvPrice, new StringExtractor<Product>() {
            @Override
            public String getStringValue(Product product, int position) {
                return "" + product.getPrice();
            }
        });

        /*ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(
                PrimaryFragment.this.getActivity(),
                android.R.layout.simple_list_item_1,
                products);*/
        // Adapter di atas diperlukan kalau tidak menggunakan fundapter

        FunDapter adapter = new FunDapter(
                PrimaryFragment.this.getActivity(),
                products,
                R.layout.product_layout,
                dictionary);

        ListView lvProduct = (ListView)view.findViewById(R.id.lvProduct);
        lvProduct.setAdapter(adapter);
        // Agar data bisa di click
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product selectedProduct = products.get(i);
                Toast.makeText(PrimaryFragment.this.getActivity(), selectedProduct.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
