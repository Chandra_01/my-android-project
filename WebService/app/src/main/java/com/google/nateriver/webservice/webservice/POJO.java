
package com.google.nateriver.webservice.webservice;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class POJO {

    @SerializedName("nama")
    @Expose
    private List<String> nama = null;
    @SerializedName("jeniskelamin")
    @Expose
    private List<String> jeniskelamin = null;
    @SerializedName("notelpon")
    @Expose
    private List<String> notelpon = null;
    @SerializedName("img_url")
    @Expose
    private List<String> imgUrl = null;

    public List<String> getNama() {
        return nama;
    }

    public void setNama(List<String> nama) {
        this.nama = nama;
    }

    public List<String> getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(List<String> jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    public List<String> getNotelpon() {
        return notelpon;
    }

    public void setNotelpon(List<String> notelpon) {
        this.notelpon = notelpon;
    }

    public List<String> getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(List<String> imgUrl) {
        this.imgUrl = imgUrl;
    }

}
