package com.google.nateriver.gratze;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        new Thread(){
            public void run(){
                try{
                    sleep(3000);
                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
