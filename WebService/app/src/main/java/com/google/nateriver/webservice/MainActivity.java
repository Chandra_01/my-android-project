package com.google.nateriver.webservice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.nateriver.webservice.webservice.POJO;
import com.google.nateriver.webservice.webservice.REST;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    // NOTE: Global Declaration
    List<String> name_list;
    List<String> jeniskelamin_list;
    List<String> notelpon_list;
    List<String> img_url_list;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Parking List");
        // NOTE : initializing
        lv=(ListView) findViewById(R.id.listView);
        restCall();
    }



    //---------------------------------REST-----------------------------------------------------//
    private void restCall() {

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(REST.BASEURL)
                .build();
        REST.api_get api = adapter.create(REST.api_get.class);

        //Defining the method
        api.getData(new Callback<POJO>() {
            @Override
            public void success( POJO  user_list_response , Response response) {
                if (user_list_response != null){

                    name_list = user_list_response.getNama();
                    jeniskelamin_list = user_list_response.getJeniskelamin();
                    notelpon_list = user_list_response.getNotelpon();
                    img_url_list = user_list_response.getImgUrl();

                    //Loads List View
                    Adapter arrayAdapter = new Adapter(getBaseContext(), name_list, jeniskelamin_list, notelpon_list, img_url_list);
                    lv.setAdapter(arrayAdapter);

                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Failed to Connect REST",""+error.getCause());
            }
        });

        //---------------------*** END REST ***-----------------------------------------------------//


    }

    public void text_msg(View view){
        Toast.makeText(this, "text clicked", Toast.LENGTH_LONG).show();
    }
}