
package com.google.nateriver.webservice.webservice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class POJOPost {

    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("jenis_kelamin")
    @Expose
    private String jenisKelamin;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

}
