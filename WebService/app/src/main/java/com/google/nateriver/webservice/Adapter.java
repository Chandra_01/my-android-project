package com.google.nateriver.webservice;

/**
 * Created by Nate River on 3/26/2018.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.nateriver.webservice.webservice.POJOPost;
import com.google.nateriver.webservice.webservice.REST;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Adapter extends BaseAdapter {

    List<String> names;
    List<String> gender;
    List<String> telp;
    List<String> img;
    Context context;
    private static LayoutInflater inflater = null;

    public Adapter(Context mainActivity, List<String> n, List<String> g, List<String> t, List<String> i) {
        names = n;
        gender = g;
        telp = t;
        img = i;
        context = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        View listItemView;
        listItemView = inflater.inflate(R.layout.list_item, null);

        //Set the name of company in the list item textview
        TextView name = (TextView) listItemView.findViewById(R.id.nama);
        name.setText(String.format("Nama : %s", names.get(position)));

        TextView gen = (TextView) listItemView.findViewById(R.id.gender);
        gen.setText(String.format("Jenis Kelamin : %s", gender.get(position)));

        TextView telpon = (TextView) listItemView.findViewById(R.id.telp);
        telpon.setText(String.format("No. Telp : %s", telp.get(position)));

        //Initialize ImageView
        ImageView imageView = (ImageView) listItemView.findViewById(R.id.imageView);
        Glide.with(context)
                .load(img.get(position))
                .override(300, 400)
                .into(imageView);

        // Set Onclick for each view
        listItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, names.get(position), Toast.LENGTH_LONG).show();
                postData(names.get(position),  gender.get(position));
            }
        });

        return listItemView;
    }

    public void postData(String name, String gender){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(REST.BASEURL)
                .build();
        REST.api_post api = adapter.create(REST.api_post.class);
        api.insertUser(
                name,
                gender,
                new Callback<POJOPost>() {
            @Override
            public void success( POJOPost  user_list_response , Response response) {
                if (user_list_response != null){
                    Toast.makeText(context, String.format("%s adalah seorang %s", user_list_response.getNama(), user_list_response.getJenisKelamin()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Failed to Connect REST",""+error.getCause());
//                Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
